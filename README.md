# HighwayEnv

A highway environment for vehicle decision-making safety performance assessment tasks. The subject vehicle admits a mixture of IDM and MOBIL.

## Example
See "example.py" for an example of simulating testing case in a frontal interactive scene with the other traffic vehicle persistently maintaining steady-state (i.e. remaining in the initialized lane at the initialized speed).