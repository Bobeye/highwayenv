from highway_env.env import HighWayEnv
from highway_env.longitudinal_ctrl import IDMEnv 
from highway_env.lateral_ctrl import MOBIL, LaneChange
from highway_env.model import Bicycle

import numpy as np 
import os 
import pickle

class IDMConfig:
	s0 = 2      # minimum distance between vehicles
	v0 = 25     # speed of vehicle in free traffic
	a = 0.73    # maximum acceleration
	b = 1.67    # comfortable deceleration
	T = 2     # safe time headway - min time to front vehicle
	delta = 4   # exponent of acceleration
	carlen = 5  # length of the vehicles (L)
	brake_max = 5   # maximum brake control
	acc_max=3
	G = 9.8     # Gravity
	theta = 0   # road flatness
	dt=0.1      # time gap
	p_scale=[0.1,0.01]
	p_bias=[0.1,0.01]
	p_noise=False

class LeadSteadySim:

	def __init__(self, env):
		self.env = env
		self.pov_lane_change = LaneChange()
		self.pov_model = Bicycle()

	def reset(self, dx, dy, v0, v1):
		h0 = np.clip(np.random.normal(size=1)[0], -np.pi/30, np.pi/30)
		h1 = np.clip(np.random.normal(size=1)[0], -np.pi/30, np.pi/30)
		s0 = [0, 0, v0, h0]
		s1 = [dx, dy, v1, h1]
		self.env.reset(s0, s1)
	
	def step(self):
		pov_state, sv_state = self.env.pov_state, self.env.sv_state
		pov_target = self.env.y2lane(pov_state[1])*self.env.lw + self.env.lw/2
		pov_steer = self.pov_lane_change.get_steer(pov_state, lateral_target=pov_target)
		pov_state_ = self.pov_model.step(pov_state, [0, pov_steer], self.env.dt)
		self.env.step(pov_state_)

	def run(self, state0, maxDuration=30, render_mode=True):
		steps = int(maxDuration/self.env.dt)
		dx, dy, v0, v1 = state0 
		collision = False
		traj = [np.array([dx, dy, v0, v1])]
		self.reset(dx, dy, v0, v1)
		for i in range(steps):
			if self.env.collision() or self.env.stop() or (self.env.state[0]<=0 and abs(self.env.state[1])>=self.env.lw) or self.env.state[0]<-self.env.carlen:
				collision = self.env.collision()
				break
			self.step()
			traj += [self.env.state]
			if render_mode:
				self.render()
		
		return traj, collision

	def render(self):
		self.env.render()


if __name__ == "__main__":
	idm = IDMEnv(config=IDMConfig())
	mobil = MOBIL(idm)
	lane_change = LaneChange()
	env = HighWayEnv(idm, mobil, lane_change, autosteer=False)
	SIM = LeadSteadySim(env)