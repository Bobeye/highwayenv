import numpy as np 
from .model import *

import matplotlib.pyplot as plt
from matplotlib import patches 

class HighWayEnv:

	def __init__(self, idm, mobil, lane_change, autosteer=True, 
				 min_state=[0, -3.7, 0, 0], max_state=[50, 3.7, 25, 25]):
		self.idm, self.mobil, self.lane_change = idm, mobil, lane_change

		self.dt = idm.dt 
		self.lw = mobil.lw 
		self.carlen = idm.carlen
		self.accel0 = 0
		self.ay0 = 0
		self.last_vy = 0

		self.autosteer = autosteer 
		self.min_state, self.max_state = np.array(min_state), np.array(max_state)

	def render(self):
		plt.clf()
		plt.plot(0, self.sv_state[1], "xr")
		plt.plot(self.state[0], self.sv_state[1]+self.state[1], "xg")
		plt.arrow(0, self.sv_state[1], self.carlen*np.cos(self.sv_state[3]), self.carlen*np.sin(self.sv_state[3]), 
					color="r", width=0.1, head_width=0.5)
		plt.xlim(-50,50)
		plt.ylim(-self.lw, self.lw*4)
		plt.pause(0.01)
	
	def reset(self, s0, s1):
		"""
			s0 = [relative x pos, relative y pos, v0, v1]
		"""
		x0, y0, v0, h0 = s0 
		x1, y1, v1, h1 = s1
		dx = x1 - x0 
		dy = y1 - y0
		if dy>=self.lw:
			y0_lane_lim = np.array([0, self.lw])
		elif dy>=0:
			y0_lane_lim = np.array([0, self.lw*2])
		elif dy>=-self.lw:
			y0_lane_lim = np.array([self.lw, self.lw*3])
		else:
			y0_lane_lim = np.array([self.lw*2, self.lw*3])
		x1 = dx + self.carlen
		x0 = 0 
		y0 = self.y2lane(np.random.uniform(y0_lane_lim[0], y0_lane_lim[1],size=1)[0])*self.lw+self.lw/2
		y0_target = self.y2lane(y0)*self.lw + self.lw/2
		y1 = y0 + dy 

		self.sv_model = Bicycle()
		self.pov_model = Bicycle()
		self.sv_state = np.array([x0, y0, v0, h0])
		self.pov_state = np.array([x1, y1, v1, h1])
		self.state = np.array([x1-x0, y1-y0, v0, v1])
		self.sv_y_target = y0_target

	def y2lane(self, y):
		return int(y//self.lw)

	def get_traffic(self, s0, sn):
		# f_old, b_old, f_new, b_new = None, None, None, None
		f_old, b_old, f_left, b_left, f_right, b_right = None, None, None, None, None, None
		for s in sn:
			same_lane = self.y2lane(s0[1])==self.y2lane(s[1]) #abs(s0[1]-s[1])<=self.lw/2
			is_right = (not same_lane) and (s0[1]>s[1])
			is_left = (not same_lane) and (s0[1]<s[1])
			in_front = s[0]-s0[0]>0
			if same_lane and in_front:
				if (f_old is None) or (s[0]<f_old[0]):
					f_old=s
			if same_lane and (not in_front):
				if (b_old is None) or (s[0]>b_old[0]):
					b_old = s
			if is_left and in_front:
				if (f_left is None) or (s[0]<f_left[0]):
					f_left = s
			if is_left and (not in_front):
				if (b_left is None) or (s[0]>b_left[0]):
					b_left = s
			if is_right and in_front:
				if (f_right is None) or (s[0]<f_right[0]):
					f_right = s
			if is_right and (not in_front):
				if (b_right is None) or (s[0]>b_right[0]):
					b_right = s
		
		return f_old, b_old, f_right, b_right, f_left, b_left

	def step(self, pov_state):
		s1_ = pov_state

		right_feasible = False if self.y2lane(self.sv_state[1])==0 else True 
		left_feasible = False if self.y2lane(self.sv_state[1])==2 else True 
		f_old, b_old, f_right, b_right, f_left, b_left = self.get_traffic(self.sv_state, [self.pov_state]) # f in the front, b follower, fnew - target forward, bnew - target backward
		accel0 = self.idm.get_acc(self.sv_state, s1=f_old)
		if self.autosteer:
			change_ok = self.mobil.change_ok(self.sv_state, f_old, b_old, f_right, b_right, f_left, b_left, self.sv_y_target, right_feasible=right_feasible, left_feasible=left_feasible)
			if change_ok=="left":
				self.sv_y_target = (int(self.sv_state[1]//self.lw)+1.5)*self.lw
			if change_ok=="right":
				self.sv_y_target = (int(self.sv_state[1]//self.lw)-0.5)*self.lw
			if change_ok=="stay":
				pass
			steer0 = self.lane_change.get_steer(self.sv_state, lateral_target=self.sv_y_target)
		else:
			steer0= 0

		self.accel0 = accel0
		s0_ = self.sv_model.step(self.sv_state, [accel0, steer0], self.dt)
		vy0 = (s0_[1]-self.sv_state[1])/self.dt 
		self.ay0 = np.clip((vy0-self.last_vy)/self.dt,-.1,.1 )
		self.last_vy = vy0

		self.sv_state = s0_
		self.pov_state = s1_
		self.state = np.array([s1_[0]-s0_[0], s1_[1]-s0_[1],s0_[2],s1_[2]])

	def collision(self):
		return self.state[0]<0 and self.state[0]>-self.carlen and abs(self.state[1])<self.lw/2

	def stop(self):
		return self.state[2]<=0

	def out_of_domain(self):
		return np.any(self.state[0]<self.min_state[0]) or np.any(self.state[0]>self.max_state[0]) or \
			   np.any(self.state[3]<self.min_state[3]) or np.any(self.state[3]>self.max_state[3])


		

		


		



		


