import numpy as np
import scipy
import matplotlib.pyplot as plt
from scipy.stats import uniform
from scipy.stats import multivariate_normal as normal
import scipy.integrate as spi
import math
import os

class LaneChange:

	def __init__(self, veh_l=5, tau_a=0.6, tau_ds=0.2, max_steering=np.pi/9):
		self.KP_HEADING = 1/tau_ds
		self.KP_LATERAL = 1/3*self.KP_HEADING
		self.MAX_STEERING_ANGLE = max_steering
		self.veh_l = veh_l

	def get_steer(self, s0, lateral_target=None):
		lateral_offset = s0[1]-lateral_target
		if abs(lateral_offset) > 1.:
			lateral_offset = np.sign(lateral_offset)*1.
		lane_coords = [s0[0], lateral_offset]
		# Lateral position control
		lateral_speed_command = - self.KP_LATERAL * lane_coords[1]
		# Lateral speed to heading
		heading_command = np.arcsin(np.clip(lateral_speed_command / not_zero(s0[2]), -1, 1))
		heading_ref = 0. + np.clip(heading_command, -np.pi/4, np.pi/4)
		# Heading control
		heading_rate_command = self.KP_HEADING * wrap_to_pi(heading_ref - s0[3])
		# Heading rate to steering angle
		steering_angle = np.arcsin(np.clip(self.veh_l / 2 / not_zero(s0[2]) * heading_rate_command, -1, 1))
		steering_angle = np.clip(steering_angle, -self.MAX_STEERING_ANGLE, self.MAX_STEERING_ANGLE)
		
		return steering_angle

def wrap_to_pi(x: float) -> float:
	return ((x + np.pi) % (2 * np.pi)) - np.pi

def not_zero(x: float, eps: float = 1e-2) -> float:
	if abs(x) > eps:
		return x
	elif x > 0:
		return eps
	else:
		return -eps

class MOBIL:
	
	def __init__(self, acc_pi, thresh=1e-3, polite=0.02, db=0.1, ss0=2, b=6, bias_right=0, 
				 veh_l=5., lane_width=3.7, p_scale=[0.1,0.01], p_bias=[0,0], p_noise=False):
		'''
		:acc_pi: assumed longitudinal controller for acceleration
		:param polite: politeness factor
		:param db: change lane incentive penalty
		:param ss0: max safe distance
		:param b: max safe braking deceleration
		:param bias_right: bias (m/s^2) to drive right
		:param veh_l: vehicle length
		:param lane_width: lane width
		'''
		self.acc_pi = acc_pi
		self.thresh, self.p, self.db, self.gap_min, self.bsave, self.bias_right, self.veh_l, self.lw = \
			thresh, polite, db, ss0, b, bias_right, veh_l, lane_width
		self.p_scale, self.p_bias, self.p_noise = p_scale, p_bias, p_noise

	def add_noise(self, state):
		state[0] = np.clip(np.random.normal(loc=state[0], scale=self.p_scale[0], size=1)[0]+self.p_bias[0],self.veh_l, np.inf)
		state[2] = np.clip(np.random.normal(loc=state[2], scale=self.p_scale[1], size=1)[0]+self.p_bias[1],0, np.inf)

		return state

	def change_ok(self, sv, f_old, b_old, f_right, b_right, f_left, b_left, current_target, right_feasible=True, left_feasible=True):
		'''
		:param sv: the current car
		:param f_old: forward car old
		:param b_old: backward car old
		:param f_right: forward car new on the right
		:param b_right: backward car new on the right
		:param f_left: forward car new on the left
		:param b_left: backward car new on the left
		:return: bool
			change or not
		'''
		if self.p_noise:
			if f_old is not None:
				f_old = self.add_noise(f_old)
			if f_right is not None:
				f_right = self.add_noise(f_right)
			if f_left is not None:
				f_left = self.add_noise(f_left)
		#assert isinstance(sv, Moveable) and isinstance(f_new, Moveable)
		# is 1 if new lane is on the right, else 0
		is_right = int(sv[1]<self.lw)
		# check whether there's enough gap to change lane
		to_right, to_left = True, True 
		if ((f_right is not None) and (f_right[0]-sv[0]<=(self.gap_min+self.veh_l/2))) or \
		   ((b_right is not None) and (sv[0]-b_right[0]<=(self.gap_min+self.veh_l/2))) or \
		   sv[1]<self.lw or not right_feasible:
		   to_right = False 
		if ((f_left is not None) and (f_left[0]-sv[0]<=(self.gap_min+self.veh_l/2))) or \
		   ((b_left is not None) and (sv[0]-b_left[0]<=(self.gap_min+self.veh_l/2))) or \
		   sv[1]>self.lw*2 or not left_feasible:
		   to_left = False 

		# check safety criterion (a > -bsave)
		# this svans we need a immediate braking after switch lane
		# if (f_right is None) and (b_right is None) and (sv[1]>self.lw):
		# 	right_advantage = 1
		# else:
		# 	right_advantage = 0
		# if (f_left is None) and (b_left is None) and (sv[1]<self.lw*2):
		# 	left_advantage = 1
		# else:
		# 	left_advantage = 0
		 
		right_advantage, left_advantage = 0, 0
		if to_right:
			b_right_acc = self.acc_pi.get_acc(b_right, s1=sv) if b_right is not None else 0
			sv_right_acc = self.acc_pi.get_acc(sv, s1=f_right) if f_right is not None else 0
			if (b_right_acc < -self.bsave or sv_right_acc < -self.bsave):
				to_right = False 
			if to_right:
				# my advantage of acceleration on lane change
				sv_acc = self.acc_pi.get_acc(sv, s1=f_old) if f_old is not None else 0
				sv_acc_adv = sv_right_acc - sv_acc + is_right * self.bias_right
				b_right_acc_disadv = self.acc_pi.get_acc(b_right, s1=f_right) - b_right_acc if b_right is not None else -b_right_acc
				# this means new back car gets more acceleration for my switch
				#   counter-intuitive case...
				if b_right_acc_disadv < 0:
					b_right_acc_disadv = 0
				right_advantage = sv_acc_adv - self.p * b_right_acc_disadv - self.db
		if to_left:
			b_left_acc = self.acc_pi.get_acc(b_left, s1=sv) if b_left is not None else 0
			sv_left_acc = self.acc_pi.get_acc(sv, s1=f_left) if f_left is not None else 0
			if (b_left_acc < -self.bsave or sv_left_acc < -self.bsave):
				to_left = False 
			if to_left:
				# my advantage of acceleration on lane change
				sv_acc = self.acc_pi.get_acc(sv, s1=f_old) if f_old is not None else 0
				sv_acc_adv = sv_left_acc - sv_acc + is_right * self.bias_right
				b_left_acc_disadv = self.acc_pi.get_acc(b_left, s1=f_left) - b_left_acc if b_left is not None else -b_left_acc
				# this means new back car gets more acceleration for my switch
				#   counter-intuitive case...
				if b_left_acc_disadv < 0:
					b_left_acc_disadv = 0
				left_advantage = sv_acc_adv - self.p * b_left_acc_disadv - self.db
		if (abs(sv[1]-current_target)<=self.thresh) and (f_old is not None):
			if (right_advantage >= left_advantage):
				if right_advantage > 0:
					return "right"
				else:
					return "stay"
			else:
				if left_advantage > 0:
					return "left"
				else:
					return "stay"
		else:
			return "stay"