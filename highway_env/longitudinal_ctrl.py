import numpy as np 
import scipy.integrate as spi

class IDMConfig:
	s0 = 2      # minimum distance between vehicles
	v0 = 25     # speed of vehicle in free traffic
	a = 0.73    # maximum acceleration
	b = 1.67    # comfortable deceleration
	T = 1     # safe time headway - min time to front vehicle
	delta = 4   # exponent of acceleration
	carlen = 4  # length of the vehicles (L)
	brake_max = 5   # maximum brake control
	acc_max=3
	G = 9.8     # Gravity
	theta = 0   # road flatness
	dt=0.01      # time gap
	p_scale=[0.1,0.01]
	p_bias=[0.1,0.01]
	p_noise=False

class IDMEnv:

	def __init__(self, config=IDMConfig()):
		self.s0 = config.s0
		self.v0 = config.v0 
		self.a = config.a 
		self.b = config.b 
		self.T = config.T 
		self.delta = config.delta 
		self.carlen = config.carlen
		self.brake_max = config.brake_max
		self.acc_max = config.acc_max
		self.G = config.G 
		self.theta = config.theta 
		self.dt = config.dt 
		self.p_scale = config.p_scale
		self.p_bias = config.p_bias
		self.p_noise = config.p_noise

	def set_speedlimit(self, v0):
		self.v0 = v0

	def get_acc(self, s0, s1=None):
		p0, q0, dp0, h0 = s0
		if s1 is not None: 
			p1, q1, dp1, h1 = s1
		else:
			p1, q1, dp1, h1 = np.inf, q0, dp0, h0
		x_, _ = self.step(np.array([p1-p0, dp0, dp1]), 0, dt=self.dt)
		if x_[1]>0:
			return (x_[1]-s0[2])/self.dt
		else:
			return 0

	def step(self, x, u, dt=0.001):
		acc = u 
		x_follow = 0 # initial position of follower vehicle
		if not self.p_noise:
			x_lead = x[0] # initial position of lead vehicle
		else:
			x_lead = np.clip(np.random.normal(loc=x[0], scale=self.p_scale[0], size=1)[0]+self.p_bias[0],self.carlen, np.inf)
		v_follow = x[1] # initial speed of follower vehicle #use 20
		if not self.p_noise:
			v_lead_init = x[2] # initial speed of lead vehicle #use 20
		else:
			v_lead_init = np.clip(np.random.normal(loc=x[2], scale=self.p_scale[1], size=1)[0]+self.p_bias[1],0, np.inf)
		# Simulation Time
		t_init = 0 # initial time
		t_final = dt # final time
		t_freq=100 # frequency

		# Initialize simulation runs
		v_init = np.array([x_follow, x_lead, v_follow]) # Initial state of simulation
		t = np.linspace(t_init, t_final, int((t_final-t_init)*t_freq+1)) # time vector for ode simulation

		acc_profile = np.array([acc])
		v_l_traj = [v_lead_init]

		for i in range(1, acc_profile.shape[0]+1):
			v_new = max(0,v_l_traj[i-1] + acc_profile[i-1])
			v_l_traj.append(v_new)

		# Simulate the ODE describing the IDM model
		xs = spi.odeint(self.f, v_init, t, args=(v_l_traj,))
		col = False
		for x in xs:
			if x[1]-x[0]<self.carlen:
				col = True 
				break
		x_ = np.array([xs[-1][1]-xs[-1][0], xs[-1][2], v_l_traj[-1]])

		return x_, col

	def f(self, v, t0, v_l_traj):
		idx = np.round(t0).astype('int')
		self.T = len(v_l_traj)-1
		if idx > self.T:
			idx=self.T

		v_l = v_l_traj[idx]
		x_f_dot = v[2]
		x_l_dot = v_l
		v_f_dot = self.a*(1 - (v[2]/self.v0)**self.delta - (self.s_star(v[2],(v[2] - v_l))/(v[1] - v[0]))**2)
		
		a_max = self.acc_max
		d_max = -self.brake_max 

		if v_f_dot > a_max:
			v_f_dot = a_max
		elif v_f_dot < d_max:
			v_f_dot = d_max


		return np.r_[x_f_dot,x_l_dot,v_f_dot]

	def s_star(self, v_f, v_f_del):
		return self.s0 + v_f*self.T + v_f*v_f_del/(2*np.sqrt(self.a*self.b))

def run(s0, s1, a1, pi0, dt=0.01):
	traj = [[s1[0]-s0[0],s0[2],s1[2]]]
	i = 0
	while (s1[0]-s0[0]>=0) and (s1[2]>=0) and (s0[2]>=0) and (i*dt<30):
		u0 = pi0.get_acc(s0, s1=s1)
		s0_ = [0, 0, s0[2]+u0*dt, 0]
		s1_ = [s1[0]+s1[2]*dt+0.5*a1*dt**2-s0[2]*dt-0.5*u0*dt**2, 0, s1[2]+a1*dt, 0]
		s0 = s0_ 
		s1 = s1_
		x = [s1[0]-s0[0],s0[2],s1[2]]
		# print (u0)
		traj += [x]
		if x[0]<=0:
			print ("col")
		i += 1
	return traj
