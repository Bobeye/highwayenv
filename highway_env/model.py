import numpy as np
import scipy
import matplotlib.pyplot as plt
from scipy.stats import uniform
from scipy.stats import multivariate_normal as normal
import scipy.integrate as spi
import math
import os

"""
Bicycle Kinetics
d_x = vcos(h + beta)
d_y = vsin(h + beta)
d_h = (v/lr)sin(beta-1)
d_v = a
beta = tan-1((lr/(lf+lr))tan(theta))
input : longitudnal accleration a
		front wheel angle theta
states: [x, y, v, h]
http://www.me.berkeley.edu/~frborrel/pdfpub/IV_KinematicMPC_jason.pdf
"""
class Bicycle():
	name = "Bicycle"

	def __init__(self, lr=2.,lf=2.,min_velocity=0, max_velocity=35):
		self.lr = lr 
		self.lf = lf 
		self.min_velocity = min_velocity
		self.max_velocity = max_velocity

	def _round(self, angle):
		while abs(angle) > 2*np.pi:
			if angle > 0:
				angle -= 2*np.pi 
			if angle < 0:
				angle += 2*np.pi
		return angle

	def _update(self,s0, delta_t, a, wh):
		[ x, y, v, yaw] = s0
		x_ = x + v * math.cos(yaw) * delta_t
		y_ = y + v * math.sin(yaw) * delta_t
		yaw_ = yaw + v / (self.lr+self.lf) * math.tan(wh) * delta_t
		v_ = v + a * delta_t
		if v_ > self.max_velocity:
			v_ = self.max_velocity
		if v_ < self.min_velocity:
			v_ = self.min_velocity

		return [x_, y_, v_, yaw_]

	def step(self, s0, u0, delta_t):
		[x0, y0, v0, h0] = s0

		return np.array(self._update(s0, delta_t, u0[0], u0[1]))